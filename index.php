﻿<?php
    require("questions.php");
?><html>
<head>
    <title></title>
    <style>
        body, html, pre { width: 99%;  white-space: pre-wrap; word-wrap: break-word; }
        span.cat { color: blue; }
        span.perg { font-weight: bold; }
        span.resp { color: green; }
        input.notas { width: 90% }
    </style>
</head>
<body>
    <pre><?php
    foreach($questions as $cat_name => $cat_questions) {
        echo "\n\n<b><span class='cat'>Categoria: {$cat_name}</span></b> \n\n";
        foreach($cat_questions as $question => $answer)
            echo " - <span class='perg'>{$question}</span> \n --> <span class='resp'>{$answer}</span>\n     <input type='text' class='notas'>\n\n";
    }
?>
    </pre>
</body>
</html>