<?php

$questions = array(

    'oop' => array(

        'Quais as vantagens da programação orientada a objetos vs procedural?' 
            => 'Organização de código complexo, facilidade de manutenção, modularidade, etc.',
        'Qual a diferença entre variáveis/funções publicas, protegidas e privadas numa classe PHP?' 
            => 'privada pode apenas ser acedida pela classe que a declarou; protegida pela classe que a declarou e todas as que a extendam/usem; publica por todas as classes e também fora da classe.',
        'Conheces algumas frameworks PHP? Quais?' 
            => 'Zend, Laravel, PHPCake, CodeIgniter',
        'Dá um exemplo de um projeto em que tenhas usado uma framework PHP.' => '',

    ),
    'php geral' => array(

        'O que é uma variável superglobal? Dê três exemplos.' 
            => 'São variáveis que estão sempre definidas em qualquer scope. $_GET (http get) $_POST (http post) $_SERVER (variáveis do servidor) $_FILE´S (no caso de upload de ficheiros) $_COOKIE $_SESSION etc.',
        'Quais os tipos de erros em PHP e suas diferenças?' 
            => 'Notice (variável por definir por exemplo), warning (mostra erro mas não bloqueia execução), fatal error (bloqueia a execução)',
        'Qual a diferença entre usar a comparação = = = e a comparação = = ?' 
            => '== verifica a comparação apenas de valor e pode tentar converter o tipo da variável, === compara valor e tipo de variável',
        'Conheces a ferramenta composer? Qual é a principal vantagem de usar esta ferramenta?' 
            => 'composer é um gestor de depêndencias, que gere e atualiza automaticamente todas as bibliotecas de que o produto depende',
        'Diferença entre unset() e unlink()?' 
            => 'unset() para indefinir variavel, unlink() para eliminar ficheiros',

    ),
    'acesso servidor' => array(

        'Como podemos aceder por SSH a uma determinada máquina?' 
            => 'Putty, WINSCP (windows), command line SSH (unix/osx)',
        'Caso seja preciso consultar as últimas 20 linhas do log de erros/acessos apache, como fazer?' 
            => '1) aceder por ssh ao servidor, 2) aceder à pasta dos logs, 3) comando "tail -n 20 errors.log" ou "watch tail -n 20 errors.log" para monitorizar o ficheiro e mostrar novas entradas live ',
        'Já usaste/conheces alguma ferramenta de controlo de versões? Quais?' 
            => 'SVN, Git',

    ),
    'front end' => array(

        'Que bibliotecas JavaScript conhece?' 
            => 'jquery vue mootools',
        'Conheces o bootstrap? Em que consiste?' 
            => 'biblioteca para desenvolvimento front-end HTML/CSS/JS, mobile-first e responsiva.',
        'Como é que desenvolves código? Usas algum IDE?' 
            => '',
        'Como é que visualizas os headers de alguns pedidos e efetuas o debug de problemas de comunicação (ajax por exemplo) ou de aspeto numa página web?'
            => 'Ferramentas do programador firefox/chrome'

    ),
    

);
